let years = [1950, 1960, 1970, 1980, 1990, 2000, 2010];

const binarySearch = (array,thingToFind,start,end) => {
    if (start>end){
        return false;
    }
    
    let mid = Math.floor((start+end)/2);
    
    if (array[mid]===thingToFind){
        return 'bingo';
    }
    else if (thingToFind < array[mid]) {
        return binarySearch(array, thingToFind, start, mid - 1);
    }
    else{
        return binarySearch(array,thingToFind,mid+1,end);
    }

}
console.log('Binary search '+  binarySearch(years,1960,0,years.length-1));