window.addEventListener("beforeunload",function(e){
    var message = "yoooooo";
    e.returnValue = message;
    return message;
});
class Show {
    constructor(title,numberOfSeasons){
        this.title = title;
        this.numberOfSeasons = numberOfSeasons;
        this.ratings = [];
        this.averageRating = 0;
    }   
    
    addRating(rating){
        this.ratings.push(rating);
        let sumOfRating = 0;
        
        for(let rating of this.ratings){
            sumOfRating += rating;
        }
        this.averageRating = sumOfRating/this.ratings.length;
    }
}
    
    let prisonBreak   = new Show('La grande Evasion',5);
    let laCasaDePapel = new Show('Bella Cia',5);

    const body = document.querySelector('body');
    const refresh = document.querySelector('#refresh');
    
    const shows = [];
    shows.push(prisonBreak,laCasaDePapel);

    const addRandomRating = () =>{
        for(let show of shows){
            if (Math.random() >= 0.2){
                const numberOfRating = Math.floor(Math.random()*4 +1);
                for (let i = 0; i< numberOfRating;i++){
                    const rat = Math.floor(Math.random()*4 +1);
                    show.addRating(rat);
                }
            }
        }
}
  
    
    
const updateShows = () => {
  for (let show of shows) {
    const newDiv = document.createElement('div');
    newDiv.classList.add('series-frame');
    const title = document.createElement('h2');
    title.innerText = show.title;
    const showDetails = document.createElement('div');
    const seasons = document.createElement('p');
    seasons.innerText = show.numberOfSeasons + ' seasons';
    const ratings = document.createElement('p');
    ratings.innerText = show.averageRating > 0 ? show.ratings.length + ' ratings\n' + show.averageRating.toFixed(1) + ' stars' : 'No ratings yet';
    showDetails.append(seasons);
    showDetails.append(ratings);
    newDiv.append(title);
    newDiv.append(showDetails);
    body.append(newDiv);
  }
};

const removeShows = () => {
  const children = [];
  for (let childNode of body.childNodes) {
    children.push(childNode);
  }
  for (let child of children) {
    if (child.tagName == 'DIV') {
      body.removeChild(child);
    }
  }
}

refresh.addEventListener('click', () => {
  removeShows();
  addRandomRating();
  updateShows();
})

