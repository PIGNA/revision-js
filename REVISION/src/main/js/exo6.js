
let getNumber1 = () => {
  return new Promise((resolve)=>{
      resolve(10)
  });
}

let getNumber2 = () => {
  return new Promise((resolve)=>{
      resolve(4)
  });
}

let compute = async () =>{
    let val1 = await getNumber1();
    let val2 = await getNumber2();
    let total = val1+val2;
   return total;
}
let result = document.getElementById('result')
compute().then(total => result.textContent=total)
