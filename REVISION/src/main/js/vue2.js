const app = new Vue({
    el: '#app',
    data: {
        costOfApples: 20,
        costOfBananas: 2,
        costOfCoconuts: 8
    },
    computed: {
        totalPrice(){
            return this.costOfApples + this.costOfBananas + this.costOfCoconuts;
        }
    }
})

  
  