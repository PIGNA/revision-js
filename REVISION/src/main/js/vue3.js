const app = new Vue({
    el:'#app',
    data:{
        shoppingCart:[
     {
          label:'Apples',
          cost:10,
          url:'/apples.html'
      },
      {
          label:'Banas',
          cost:3,
          url:'/bananas.html'
      },
      {
          label:'Coconuts',
          cost:12,
          url:'/coconuts.html'

            
        }]
    },
    computed:{
        totalPrice(){
            let total = 0
            this.shoppingCart.forEach(item=>{
                total+=item.cost;
            })
            return total;
        }
        
    }
})