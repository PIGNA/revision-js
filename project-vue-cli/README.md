# project-vue-cli
```
npm install -g @vue/cli
vue --version
vue create project-vue-cli; row down enter
vue add router; no
vue add vuex


```

## Hooks
```
    beforeCreate

    created

    beforeMount

    mounted

    beforeDestroy

    destroyed
```
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
# Analyse du state de Cafe with a Vue

## Données Globales

Voici les données qui ont le plus besoin d'être partagées entre plusieurs composants au fur et à mesure où l'application grossit.

- Company Name
- WatchStore
- Shopping Cart

## Données Locales

Les informations ci-dessous sont typiquement contenues dans une seule page donc n'ont pas besoin d'être partagées entre plusieurs composants.

- Contact

**Note**: Il s'agit ici d'une interprétation possible de comment les données sont organisées. Il y a plusieurs moyens de le faire, et sentez vous libre de tester différentes idées quand vous construisez des applications.

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### compile your code
npm install webpack webpack-cli --save-dev

### read the lesson https://openclassrooms.com/fr/courses/5543061-ecrivez-du-javascript-pour-le-web/5577766-compilez-et-executez-votre-code
### to manage your tuto
Description de l'architechture
1 - définition des rout (url)
###/router.index.js
2 - Stockage des objects avec vuex
###/store/index.js