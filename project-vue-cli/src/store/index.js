import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
		watchCompanyName: "MP-WATCH",
		shoppingCart: 0,
                watchList: [
                    {
                        name: "LE FORBAN",
                        image : {
                            source: "/images/LE-FORBAN.jpg",
                            alt : "le forban"
                        },
                        inStock: false,
                        price : 250
                    },
                    {
                        name: "AMPORIO ARMANI",
                        image : {
                            source: "/images/emporio-armani.jpg",
                            alt: "emporio armani"
                        },
                        inStock: true,
                        quantity : 2,
                        price : 300

                    },
                    {
                        name: "LE FORBAN ANTI CHOC",
                        image: {
                            source: "/images/LE-FORBAN-ANTI-CHOC.jpg",
                            alt: "LE FORBAN ANTI CHOC"
                        },
                        inStock: true,
                        quantity : 1,
                        price : 175
                    },
                    {
                         name: "LE FORBAN MALOUINE",
                         image: {
                            source: "/images/forban-malouine-noire.jpg",
                            alt: "LE FORBAN MALOUINE"
                        },
                        inStock: false,
                        quantity: 1,
                        price: 200
                    },
                    {
                         name: "FOSSIL",
                         image: {
                            source: "/images/montre-fossil-homme-automatique-acier-cuir-marron.jpeg",
                            alt: "FOSSIL"
                        },
                        inStock: false,
                        price: 90
                    }
                ]
            },
    getters: {
        copyright: (state) => {
            const currentYear = new Date().getFullYear()
            return `Copyright ${state.watchCompanyName} ${currentYear}`
        }
    },
    mutations: {
            ADD_ITEMS_TO_SHOPPING_CART(state, amount) {
                state.shoppingCart += amount
            },
            CLEAN_SHOPPING_CART(state) {
               state.shoppingCart *=0;
            }
        },
  actions: {
   updateShoppingCart({ commit }, amount) {
                  commit("ADD_ITEMS_TO_SHOPPING_CART", amount)
              },
   cleanShoppingCart(context) {
                 context.commit("CLEAN_SHOPPING_CART")
                }
              },
  modules: {}
})
